package ds2.gradle.plugins.gitinfo

import ds2.gradle.plugins.gitinfo.tasks.CreatePropertiesFileTask
import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.junit.Ignore
import org.junit.Test

import static org.junit.Assert.assertTrue

class PluginTest {
    @Test
    void demo_plugin_should_add_task_to_project() {
        Project project = ProjectBuilder.builder().build()
        project.getPlugins().apply 'ds2.gitinfo'
        assertTrue(project.tasks.createScmPropertyFile instanceof CreatePropertiesFileTask)
    }

    @Test
    @Ignore
    void addMetaToProject() {
        Project project = ProjectBuilder.builder().build()
        project.getPlugins().apply 'ds2.gitinfo'
        assertTrue(project.tasks.createScmPropertyFile instanceof CreatePropertiesFileTask)
        CreatePropertiesFileTask task = project.tasks.createScmPropertyFile
        task.execute()
    }

}
