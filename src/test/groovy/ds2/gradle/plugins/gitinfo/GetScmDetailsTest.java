package ds2.gradle.plugins.gitinfo;

import ds2.gradle.plugins.gitinfo.api.ScmDetails;
import ds2.gradle.plugins.gitinfo.impl.ScmDetailsImpl;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class GetScmDetailsTest {
    private ScmDetails to;

    @Before
    public void onMethod() {
        to = new ScmDetailsImpl();
    }


    @Test
    public void testGetId() {
        Path path = Paths.get(".").normalize().toAbsolutePath();
        String revId = to.getScmRevision(path, "HEAD", 7);
        assertNotNull(revId);
        assertTrue(revId.length() > 0);
    }

    @Test
    public void testGetBranchName() {
        Path path = Paths.get(".").normalize().toAbsolutePath();
        String revId = to.getBranchName(path);
        assertNotNull(revId);
        assertTrue(revId.length() > 0);
    }
}
