package ds2.gradle.plugins.gitinfo.tasks

import ds2.gradle.plugins.gitinfo.api.GitInfoExtension
import org.gradle.api.Task
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction

import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardOpenOption
import java.time.Instant

class CreatePropertiesFileTask extends AbstractGitInfoTask {
    @OutputFile
    @Optional
    File targetFile
    @Input
    GitInfoExtension extension

    CreatePropertiesFileTask() {
        super()
        setGroup("Build")
        setDescription("Creates a java property file at the configured location")
        dependsOn 'installScmInfo'
    }

    @Override
    Task configure(Closure closure) {
        if(targetFile==null){
            logger.debug("target file not set. Will use default value.")
        }
        return super.configure(closure)
    }

    @TaskAction
    void createPropertiesFile() {
        logger.debug("Extension data is {}", extension)
        //get src directory of generated srcSet
        Path propertiesFile = Paths.get(extension.generatePropertiesIn)
        if (targetFile != null) {
            propertiesFile = targetFile.toPath()
        } else {
            targetFile = propertiesFile.toFile()
        }
        Files.createDirectories(propertiesFile.parent)
        project.extensions.extraProperties.set("thisProp", "hello world")
        try {
            BufferedWriter bw = Files.newBufferedWriter(propertiesFile, StandardCharsets.UTF_8, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE, StandardOpenOption.WRITE,)
            bw.writeLine("# File generated on " + (Instant.now()))
            bw.write("scm.revision=")
            bw.write(project.property(extension.revisionPropertyName))
            bw.write('\n')
            bw.write("scm.branch=")
            bw.write(project.property(extension.branchPropertyName))
            bw.write('\n')
            bw.write(extension.builddatePropertyName)
            bw.write("=")
            bw.write(project.property(extension.builddatePropertyName))
            bw.write('\n')
            bw.close()
        } finally {
            //nothing
        }
    }
}
