package ds2.gradle.plugins.gitinfo.tasks

import ds2.gradle.plugins.gitinfo.api.ScmDetails
import ds2.gradle.plugins.gitinfo.impl.ScmDetailsImpl
import org.gradle.api.DefaultTask
import org.gradle.api.Task

class AbstractGitInfoTask extends DefaultTask {
    protected ScmDetails scmDetails

    AbstractGitInfoTask() {
        super()
    }

    @Override
    Task configure(Closure closure) {
        logger.info("Preparing scmdetails..")
        scmDetails = new ScmDetailsImpl()
        return super.configure(closure)
    }
}
