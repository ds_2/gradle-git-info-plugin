package ds2.gradle.plugins.gitinfo.tasks

import ds2.gradle.plugins.gitinfo.api.GitInfoExtension
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction

class PrintScmDetailsTask extends DefaultTask {
    @Input
    GitInfoExtension infoExtension

    PrintScmDetailsTask() {
        super()
        setGroup("Build")
        setDescription("Prints some scm details on the console/logger; nothing else")
        dependsOn 'installScmInfo'
    }

    @TaskAction
    void printScmData() {
        String branchName = project.property(infoExtension.branchPropertyName)
        String revId = project.property(infoExtension.revisionPropertyName)
        System.out.println("Revision: " + revId + " on " + branchName)
    }
}
