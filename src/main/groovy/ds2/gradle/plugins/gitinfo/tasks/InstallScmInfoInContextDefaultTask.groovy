package ds2.gradle.plugins.gitinfo.tasks

import ds2.gradle.plugins.gitinfo.api.GitInfoExtension
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction

import java.nio.file.Path
import java.time.Instant

class InstallScmInfoInContextDefaultTask extends AbstractGitInfoTask {
    @Input
    GitInfoExtension extension
    @Input
    String branchName
    @Input
    String revisionId

    InstallScmInfoInContextDefaultTask() {
        super()
        setGroup("Build")
        setDescription("Installs some scm properties into the project, for future filtering of resources")
    }

    @TaskAction
    void installScmDetails() {
        logger.debug("Starting the scm installer..")
        Path projectPath = project.projectDir.toPath()
        branchName = scmDetails.getBranchName(projectPath)
        revisionId = scmDetails.getScmRevision(projectPath, extension.referenceName, extension.countCommitId)
        logger.debug("So far, branch name is {}, and revId={}", branchName, revisionId)

        if (!project.hasProperty(extension.branchPropertyName)) {
            project.extensions.extraProperties.set(extension.branchPropertyName, "")
        }
        if (!project.hasProperty(extension.branchPropertyName)) {
            throw new IllegalStateException("No property named " + extension.branchPropertyName + " found in ext properties. Forgot to set?")
        }
        project.properties.put(extension.branchPropertyName, branchName)

        if (!project.hasProperty(extension.revisionPropertyName)) {
            throw new IllegalStateException("No property named " + extension.revisionPropertyName + " found in ext properties. Forgot to set?")
        }
        project.properties.put(extension.revisionPropertyName, revisionId)
        project.properties.put(extension.builddatePropertyName, Instant.now().toString())
        logger.debug("Done with installation")
    }
}
