package ds2.gradle.plugins.gitinfo.api

import java.nio.file.Path

interface ScmDetails {
    String getBranchName(Path gitPath)

    String getScmRevision(Path gitPath, String referenceName, int maxRevLength)
}