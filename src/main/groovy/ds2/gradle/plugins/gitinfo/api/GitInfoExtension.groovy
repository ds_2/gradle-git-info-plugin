package ds2.gradle.plugins.gitinfo.api

import org.gradle.execution.commandline.TaskConfigurationException

class GitInfoExtension implements Serializable {
    String versionPattern
    int countCommitId = 7
    String referenceName = "HEAD"
    String branchPropertyName = "scm.branchName"
    String revisionPropertyName = "scm.revision"
    String builddatePropertyName = "scm.buildDate"
    String generatePropertiesIn = "build/generated/scmDetails.properties"


    void validate() {
        if (countCommitId < 5) {
            throw new TaskConfigurationException("The count is too less: " + countCommitId)
        }
        if (referenceName == null || referenceName.trim().length() == 0) {
            throw new TaskConfigurationException("The branch reference name is not set!")
        }
    }

    @Override
    String toString() {
        final StringBuilder sb = new StringBuilder("GitInfoExtension{")
        sb.append("versionPattern='").append(versionPattern).append('\'')
        sb.append(", countCommitId=").append(countCommitId)
        sb.append(", referenceName='").append(referenceName).append('\'')
        sb.append(", branchPropertyName='").append(branchPropertyName).append('\'')
        sb.append(", revisionPropertyName='").append(revisionPropertyName).append('\'')
        sb.append(", builddatePropertyName='").append(builddatePropertyName).append('\'')
        sb.append(", generatePropertiesIn='").append(generatePropertiesIn).append('\'')
        sb.append('}')
        return sb.toString()
    }
}
