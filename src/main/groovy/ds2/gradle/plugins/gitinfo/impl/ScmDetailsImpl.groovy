package ds2.gradle.plugins.gitinfo.impl

import ds2.gradle.plugins.gitinfo.api.ScmDetails
import org.eclipse.jgit.internal.storage.file.FileRepository
import org.eclipse.jgit.lib.Ref
import org.eclipse.jgit.lib.Repository
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.lang.invoke.MethodHandles
import java.nio.file.Files
import java.nio.file.Path

//import org.gradle.internal.impldep.org.eclipse.jgit.internal.storage.file.FileRepository
//import org.gradle.internal.impldep.org.eclipse.jgit.lib.Ref
//import org.gradle.internal.impldep.org.eclipse.jgit.lib.Repository
class ScmDetailsImpl implements ScmDetails {
    private static final Logger LOG = LoggerFactory.getLogger(ScmDetailsImpl.class)

    private void assertExists(Path p) {
        if (p != null && Files.isDirectory(p)) {
            //good
        } else {
            throw new IllegalArgumentException("The given path " + p + " is not a directory!");
        }
    }

    private Path getGitRecursivePath(Path somePath) {
        Path normalizedPath = somePath.normalize()
        LOG.debug("Checking given path {} for containing a git dir..", normalizedPath)
        if (!normalizedPath.endsWith(".git")) {
            normalizedPath = normalizedPath.resolve(".git")
        }
        LOG.debug("Will check for existence of {}..", normalizedPath)
        if (Files.exists(normalizedPath)) {
            LOG.debug("found :)")
            return normalizedPath
        }
        LOG.debug("No git dir found in {}, will try with parent..", normalizedPath)
        Path thisParent = normalizedPath.parent
        LOG.debug("Parent could be {}", thisParent)
        if (thisParent.getParent() != null) {
            return getGitRecursivePath(thisParent.parent)
        }
        LOG.debug("No parent so far available. Will return null")
        return null
    }

    String getBranchName(Path gitPath) {
        LOG.debug("Starting checking branch name of path {}", gitPath);
        gitPath = gitPath.normalize()
        gitPath = getGitRecursivePath(gitPath)
        if (!gitPath.getFileName().toString().equalsIgnoreCase(".git")) {
            LOG.debug("Need to add git control directory..")
            gitPath = gitPath.resolve(".git")
        }
        assertExists(gitPath)
        try {
            Repository repository = new FileRepository(gitPath.toFile())
            return repository.getBranch();
        } catch (IOException e) {
        }
        return null
    }

    String getScmRevision(Path gitPath, String referenceName, int maxRevLength) {
        LOG.debug("Starting checking branch name of path {}", gitPath)
        gitPath = getGitRecursivePath(gitPath.normalize())
        if (!gitPath.getFileName().toString().equalsIgnoreCase(".git")) {
            LOG.debug("Need to add git control directory..")
            gitPath = gitPath.resolve(".git")
        }

        assertExists(gitPath)
        try {
            Repository repository = new FileRepository(gitPath.toFile())
            Ref head = repository.findRef(referenceName)
            if (head != null) {
                String revFull = head.getObjectId().getName()
                if (maxRevLength > 0) {
                    revFull = revFull.substring(0, Math.min(maxRevLength, revFull.length()))
                }
                return revFull
            }
        } catch (IOException e) {
            LOG.error("IO error when getting details about git!", e)
        }
        LOG.warn("no ref found!")
        return null
    }
}
