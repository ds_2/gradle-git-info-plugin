package ds2.gradle.plugins.gitinfo

import ds2.gradle.plugins.gitinfo.api.GitInfoExtension
import ds2.gradle.plugins.gitinfo.tasks.CreatePropertiesFileTask
import ds2.gradle.plugins.gitinfo.tasks.InstallScmInfoInContextDefaultTask
import ds2.gradle.plugins.gitinfo.tasks.PrintScmDetailsTask
import org.gradle.api.Plugin
import org.gradle.api.Project

class GitInfoPlugin implements Plugin<Project> {
    private String sourceSetName = "gitInfo"

    @Override
    void apply(Project project) {
        GitInfoExtension gitInfoExtension = null
        //project.rootProject.tasks.
        if (!project.extensions.findByType(GitInfoExtension.class)) {
            gitInfoExtension = project.extensions.create("gitInfo", GitInfoExtension.class)
        } else {
            gitInfoExtension = project.extensions.findByType(GitInfoExtension.class)
        }

        project.tasks.create("printScmDetails", PrintScmDetailsTask.class) {
            infoExtension = gitInfoExtension
        }
        project.tasks.create("installScmInfo", InstallScmInfoInContextDefaultTask) {
            extension = gitInfoExtension
        }
        project.tasks.create("createScmPropertyFile", CreatePropertiesFileTask) {
            extension = gitInfoExtension
        }
//        project.getRootProject().sourceSets {
//            generated {
//                resources.srcDir "src/generated/resources"
//                output.dir("${buildDir}/generated/bin", builtBy: 'generateMyResources')
//            }
//        }

    }
}
