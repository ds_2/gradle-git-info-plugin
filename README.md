# DS/2 Gradle Git Info Plugin

A gradle task/plugin to get the git info data from the current project.

## Apply plugin (consumer version)

Simply add this:

    plugins {
        id 'ds2.gitinfo' version '0.0.1' //please check the version you want to use
    }

## Configuration

Use the extension:

    gitInfo {
        countCommitId = 7
        referenceName = 'HEAD'
    }
    ...
    allprojects {
        apply plugin: ...
        apply plugin: 'ds2.gitinfo'
        ...
    }

## Apply plugin (developer version)

Add this to your build.gradle root file:

    buildscript {
        repositories {
            mavenLocal()
            jcenter()
        }
        dependencies {
            classpath 'ds2.gradle.plugins.gitinfo:gradle-git-info-plugin:0.0.1-SNAPSHOT'
        }
    }
    
    apply plugin: 'ds2.gitinfo'

This will apply the developer version.

## Release plugin (developer version)

Run

    ./gradlew :clean :release

